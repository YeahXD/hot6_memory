package com.example.android.mapexample4;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class MemberList extends Activity {

	public Button returnmap;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    setContentView(R.layout.memberlist);
	    
	    ListView lv = (ListView)findViewById(R.id.list_list);
	    
	    DBAdapter adb = new DBAdapter(this, DBAdapter.SQL_CREATE_MEMBER, "member");
	    
	    adb.open();
	    
	    String columns[] = {"name", "address"};
	    
	    Cursor c = adb.selectTable(columns, null, null, null, null, null);
	    
	    ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String, String>>();
	    
	    if(c.moveToFirst()) // ���� row�ϳ��� ������ �Ǿ����� if�� �����. ������ ����ȵ�.
	    {
	    	do {
	    		HashMap<String,String> map = new HashMap<String,String>();
	    		
	    		map.put("name", c.getString(0)); // first column data
	    		map.put("address", c.getString(1)); // second column data
	    		list.add(map);
	    		
	    	} while(c.moveToNext());	    	
	    }
	    
	    SimpleAdapter ap =	new SimpleAdapter(
	    		this, 
	    		list, 
	    		android.R.layout.simple_list_item_2, 
	    		new String[] {"name", "address"},
	    		new int[] {android.R.id.text1, android.R.id.text2});
	   	    		
	   	lv.setAdapter(ap);
		
	   	returnmap = (Button) findViewById(R.id.button1);
		returnmap.setOnClickListener(new OnClickListener() {
	    	public void onClick(View v) {
	    		Intent myI2 = new Intent(MemberList.this, Myloc.class );
	    		//myI2.putExtra("lat", list.lastIndexOf(address));
	    		startActivity(myI2);
	    	}
	    });
		
	    adb.close();

	}
	
}
