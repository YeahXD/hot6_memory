package com.example.android.mapexample4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBAdapter  {
	
	private DatabaseHelper mHelper;
	private SQLiteDatabase db;
	private static final String DATABASE_NAME="memberdb";
	private static final int DATABASE_VERSION=2;
	private static String SQL_TABLE_CREATE;
	private static String TABLE_NAME;
	
	public static final String SQL_CREATE_MEMBER=
		"create table member ( no integer primary key autoincrement," 
		+ " name text not null,"
		+ " address text not null"
		+ ")";
	
	private final Context cxt;
	
		// inner class
		// db create, update, open, close �� �����ִ� ��ü
		private static class DatabaseHelper extends SQLiteOpenHelper {
			DatabaseHelper(Context cxt) {
				super(cxt, DATABASE_NAME, null, DATABASE_VERSION);
			}

			@Override
			public void onCreate(SQLiteDatabase db) {
				// TODO Auto-generated method stub
				db.execSQL(SQL_TABLE_CREATE);
			}

			@Override
			public void onOpen(SQLiteDatabase db) {
				// TODO Auto-generated method stub
				super.onOpen(db);
			}

			@Override
			public void onUpgrade(SQLiteDatabase db, int oldVersion,
					int newVersion) {
				// TODO Auto-generated method stub
				db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
				onCreate(db);
			}
			
		}
		
		public DBAdapter(Context cxt, String sql, String tName) {
			this.cxt=cxt;
			SQL_TABLE_CREATE=sql;
			TABLE_NAME=tName;
		}
		
		// db open
		public DBAdapter open()throws SQLException {
			mHelper = new DatabaseHelper(cxt);
			db = mHelper.getWritableDatabase();
			// helper�� �˾Ƽ� ������ create���ٲ��� open���ٲ���
			// version�ٸ��� �˾Ƽ� upgrade������ ���̴�.
			return this;
		}
		
		public void close() {
			mHelper.close();
		}
		
		// insert
		public long insertTable(ContentValues values) {
			return db.insert(TABLE_NAME, null, values); // null column : ��� column
		}
		
		// delete
		public boolean deleteTable(String pkColumn, long pkData) { 
			// ��� row�� delete�Ǿ�ٰ� ������ �����Ѵ�. 
			// delete�� �����ߴ����� Ȯ���ϱ� ���� return������ 0���� ū���� Ȯ��.
			// ?�κ��� null�� ��.
			return db.delete(TABLE_NAME, pkColumn+"="+pkData, null)>0;
		}
		
		// select
		public Cursor selectTable(String[] columns, String selection, 
				String[] selectionArgs, String groupBy, String having, String orderBy)
		{
			// where������ ���� �����
			return db.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
		}
		
		// update
		public boolean updateTable(ContentValues values, String pkColumn, long pkData) {
			return db.update(TABLE_NAME, values, pkColumn+"="+pkData, null)>0;
		}
}
