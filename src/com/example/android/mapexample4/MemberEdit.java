package com.example.android.mapexample4;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MemberEdit extends Activity implements OnClickListener{

	private Button bt;
	private EditText et1;
	private EditText et2;
	public String str1;
	public String str2;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	
	    setContentView(R.layout.memberedit);

	    Intent intent = getIntent();
	    str1 = intent.getExtras().getString("lat");
	    str2 = intent.getExtras().getString("log");
	    
	    
	    
	    bt = (Button)findViewById(R.id.edit_button);
	    et1 = (EditText)findViewById(R.id.edit_text1);
	    et2 = (EditText)findViewById(R.id.edit_text2);
	    et2.setText("위도: "+str1+"\n경도: "+str2);
	    
	    bt.setOnClickListener(this);
	    
	}
	
	public void onClick(View v)
	{
		if(v==bt) {
			String name = et1.getText().toString();
			String addr = et2.getText().toString();
			
			DBAdapter adb = new DBAdapter(this, DBAdapter.SQL_CREATE_MEMBER, "member");
			adb.open();
			
			ContentValues values = new ContentValues();
			values.put("name", name); // 사용자가 edit text에서 넣어주는 변수를 contentvalue에 넣는다.
			values.put("address", addr);
			
			adb.insertTable(values); // table을 완성한다.
			
			adb.close();
			
			Intent intent = new Intent(this, MemberList.class); // member list activity실행
			startActivity(intent);
		}
	}
}
