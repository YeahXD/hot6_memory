package com.example.android.mapexample4;

import java.util.Date;
import java.util.List;

import com.example.android.mapexample4.R;

import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	
	public static final String TAG = "MainActivity";

	TextView nameText;
	
	Button myloc;
	Button search;
	Button twitsearch;


	
	Handler mHandler = new Handler();
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        
    	startActivity(new Intent(this, SplashActivity.class));
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize(); 
    	
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        search = (Button) findViewById(R.id.search);
        search.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		
        		Intent myI2 = new Intent(getApplicationContext(), Search.class );

        		startActivity(myI2);

        	}
        });
        
        twitsearch = (Button) findViewById(R.id.twitsearch);
        twitsearch.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		
        		Intent myI3 = new Intent(getApplicationContext(), TwitterSearchActivity.class );

        		startActivity(myI3);

        	}
        });
        
        myloc = (Button) findViewById(R.id.myloc);
        myloc.setOnClickListener(new OnClickListener() {
        	public void onClick(View v) {
        		
        		Intent myI = new Intent(getApplicationContext(), Myloc.class );

        		startActivity(myI);

        	}
        });
        
       

        nameText = (TextView) findViewById(R.id.nameText);


			

        
    }
    /**
     * ���÷��� ǥ���ϴ� �Ͱ� �ʱ�ȭ�� ���ÿ� �����Ű�� ���Ͽ� ������ ó��
     *
     */
    private void initialize()
    {
        InitializationRunnable init = new InitializationRunnable();
        new Thread(init).start();
    }


    /**
     * �ʱ�ȭ �۾� ó��
     *
     */
    class InitializationRunnable implements Runnable
    {
        public void run()
        {
            // ���⼭���� �ʱ�ȭ �۾� ó��
            // do_something
        }
    }

  
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
