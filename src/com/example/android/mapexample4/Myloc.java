package com.example.android.mapexample4;


import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
 
public class Myloc extends FragmentActivity implements LocationListener {
 
    GoogleMap googleMap;

    double ilat1;
    double ilat2;
    String silat1;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mylocation);
        
        
        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
 
        // Showing status
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available
 
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
 
        }else { // Google Play Services are available
 
            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
 
            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();
 
            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);
 
            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
 
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();
 
            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);
 
            // Getting Current Location
            Location location = locationManager.getLastKnownLocation(provider);
 
            if(location!=null){
                onLocationChanged(location);
            }
            locationManager.requestLocationUpdates(provider, 20000, 0, this);
        }
        imageV();
        mark();
    }
    @Override
    public void onLocationChanged(Location location) {
 
        TextView tvLocation = (TextView) findViewById(R.id.tv_location);
 
        // Getting latitude of the current location
        double latitude = location.getLatitude();
 
        // Getting longitude of the current location
        double longitude = location.getLongitude();
 
        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);
 
        // Showing the current location in Google Map
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
 
        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
 
        // Setting latitude and longitude in the TextView tv_location
        tvLocation.setText("Latitude:" +  latitude  + ", Longitude:"+ longitude );
 
    }
 
    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    private void imageV() {
    	LatLng loc = new LatLng(37.6314925,126.8260696);
		MarkerOptions markerOptions2 = new MarkerOptions();
		markerOptions2.icon(BitmapDescriptorFactory.fromResource(R.drawable.camera));
		markerOptions2.position(loc);
		markerOptions2.title("이 위치의 이미지를 보시려면 저를 눌러주세요");
		markerOptions2.snippet("위도:"+loc.latitude+",경도:"+loc.longitude);
		googleMap.animateCamera(CameraUpdateFactory.newLatLng(loc));
		googleMap.addMarker(markerOptions2);
		
		LatLng loc5 = new LatLng(37.6019332,126.960904);
		MarkerOptions markerOptions5 = new MarkerOptions();
		markerOptions5.icon(BitmapDescriptorFactory.fromResource(R.drawable.camera));
		markerOptions5.position(loc5);
		markerOptions5.title("이 위치의 이미지를 보시려면 저를 눌러주세요");
		markerOptions5.snippet("위도:"+loc.latitude+",경도:"+loc.longitude);
		googleMap.animateCamera(CameraUpdateFactory.newLatLng(loc));
		googleMap.addMarker(markerOptions5);
		
		LatLng loc2 = new LatLng(37.60234128,126.95693496);
		MarkerOptions markerOptions3 = new MarkerOptions();
		markerOptions3.icon(BitmapDescriptorFactory.fromResource(R.drawable.camera));
		markerOptions3.position(loc2);
		markerOptions3.title("이 위치의 이미지를 보시려면 저를 눌러주세요");
		markerOptions3.snippet("위도:"+loc.latitude+",경도:"+loc.longitude);
		googleMap.animateCamera(CameraUpdateFactory.newLatLng(loc));
		googleMap.addMarker(markerOptions3);

    }
    private void mark() {
		googleMap = ((SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map)).getMap();
		
		googleMap.setOnMapClickListener(new OnMapClickListener() {
			@Override
			public void onMapClick(LatLng latLng) {
				MarkerOptions markerOptions = new MarkerOptions();
				markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin));
				markerOptions.position(latLng);
				markerOptions.title("트위터에 글을 쓰려면 저를 누르세요.");
				markerOptions.snippet("위치 => 위도:"+latLng.latitude+",경도:"+latLng.longitude);
				
				ilat1 = latLng.latitude;
				ilat2 = latLng.longitude;
				silat1 = Double.toString(ilat1);
				
				googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
				googleMap.addMarker(markerOptions);
				}
			});
		 googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
			@Override
			public void onInfoWindowClick(Marker arg0){
				
				Intent myI7 = new Intent(getApplicationContext(), Twit.class );
				myI7.putExtra("lat", Double.toString(ilat1));
				myI7.putExtra("log", Double.toString(ilat2));
        		startActivity(myI7);

			}
		});
		googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

	}
}
